import sqlite3

def create_tables():
    conn = sqlite3.connect('Hospital Database.db')
    c = conn.cursor()
    c.execute("""CREATE TABLE Hospital (
                Hospital_Id integer PRIMARY KEY,
                Hospital_Name text NOT NULL,
                Bed_Count integer NOT NULL
                )""")
    c.execute("""CREATE TABLE Doctor(
            Doctor_Id integer PRIMARY KEY,
            Doctor_Name text NOT NULL,
            Hospital_Id integer NOT NULL,
            Joining_Date text NOT NULL,
            Speciality text NOT NULL,
            Salary integer NOT NULL,
            Experience text,
            FOREIGN KEY (Hospital_Id) REFERENCES Hospital (Hospital_Id)
            )""")

    conn=sqlite3.connect('Hospital Database.db')
    c=conn.cursor()
    hosp_records=[
        (1,'Mayo Clinic',200),
        (2,'Cleveland Clinic', 400),
        (3,'John Hopkins', 1000),
        (4,'UCLA Medical Center',1500)
                    ]
    c.executemany("INSERT INTO Hospital (Hospital_Id, Hospital_Name, Bed_Count) VALUES (?,?,?)",hosp_records)
    doc_records=[
        (101, 'David', 1, '2005-02-10', 'Pediatric', 40000),
        (102, 'Michael', 1, '2018-07-23', 'Oncologist', 20000),
        (103, 'Susan', 2, '2016-05-19', 'Garnacologist', 25000),
        (104, 'Robert', 2, '2017-12-28', 'Pediatric', 28000),
        (105, 'Linda', 3, '2004-06-04', 'Garnacologist', 42000),
        (106, 'William', 3, '2012-09-11', 'Dermatologist', 30000),
        (107, 'Richard', 4, '2014-08-21', 'Garnacologist', 32000),
        (108, 'Karen', 4, '2011-10-17', 'Radiologist', 30000),
                ]
    c.executemany("INSERT INTO Doctor (Doctor_Id,Doctor_Name,Hospital_Id,Joining_Date,Speciality,Salary) VALUES (?,?,?,?,?,?)",doc_records)

    conn.commit()
    conn.close()


def doctor_list(speciality, salary):
    conn = sqlite3.connect('Hospital Database.db')
    c = conn.cursor()
    c.execute("SELECT * FROM Doctor WHERE Speciality = (?) AND Salary >=(?)",(speciality,salary))
    for item in c.fetchall():
        print(item)


def hos_id(h_id):
    conn = sqlite3.connect('Hospital Database.db')
    c = conn.cursor()
    c.execute("SELECT Doctor.Doctor_Name, Hospital.Hospital_Name FROM Doctor INNER JOIN Hospital ON Doctor.Hospital_Id=Hospital.Hospital_Id WHERE Doctor.Hospital_Id=(?)",(h_id,))
    for item in c.fetchall():
        print(item)

