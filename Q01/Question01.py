import sqlite3


def create_table():
    # Create and connect to a database
    db_conn = sqlite3.connect('CARS_Database.db')

    # Create a cursor
    cursor = db_conn.cursor()

    # Create a table called CARS with two columns, NAME OF CAR and NAME OF OWNER
    create_query = """
                CREATE TABLE CARS(
                Name_of_car text NOT NULL,
                Name_of_owner text NOT NULL)
                """
    cursor.execute(create_query)

    db_conn.commit()
    db_conn.close()


def records():
    # Create and connect to a database
    db_conn = sqlite3.connect('CARS_Database.db')

    # Create a cursor
    cursor = db_conn.cursor()

    # Add 10 records into CARS table
    enter_records = "INSERT INTO CARS (Name_of_car, Name_of_owner) VALUES (?,?)"

    cursor.execute(enter_records, ('Ford Ranger','John Smith'))
    cursor.execute(enter_records, ('Mini Cooper','Raymond Holt'))
    cursor.execute(enter_records, ('Toyota Prius','Kevin Sparks'))
    cursor.execute(enter_records, ('WagonR','Betty Cooper'))
    cursor.execute(enter_records, ('Honda Civic','Amy Elliot'))
    cursor.execute(enter_records, ('Volkswagen Beetle','Lilly Aldrin'))
    cursor.execute(enter_records, ('Chevrolet Volt','Jane Dunken'))
    cursor.execute(enter_records, ('Ford Fiesta','Mary Beth'))
    cursor.execute(enter_records, ('Buick Cascada','Kirsten W'))
    cursor.execute(enter_records, ('Nissan Versa','Joe Fox'))

    db_conn.commit()
    db_conn.close()


def retrieve():
    # Create and connect to a database
    db_conn = sqlite3.connect('CARS_Database.db')

    # Create a cursor
    cursor = db_conn.cursor()

    cursor.execute("SELECT * FROM CARS")

    for item in cursor.fetchall():
        print(item)

    db_conn.commit()
    db_conn.close()